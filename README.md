# Handling data from cinemaworld and filmworld API by Django
## Requirements
* Build a web app to allow customers to get the cheapest price for movies from these two providers in a timely manner.
* Design a solution to have a functioning app when not all of the providers are functioning at 100 %.
* The API token provided to you should not be exposed to the public.
## Assumption
* The provider did not provide the webhook for third-party user.
* The tolerance of waiting time for user to loading a web page is under 2 seconds.
* The two providers are not providing exact the same movies some time.
* The available data size is not very large, otherwise database is required.

## Solution
* Timely: Create a class [MovieHandler](https://bitbucket.org/brucematrix/webjet/src/master/movies/moviehandler.py) in moviehandler.py to get timely data from the APIs of both providers, and render the unique movies data by [movies_list](https://bitbucket.org/brucematrix/webjet/src/master/movies/views.py) and render different price by [movie_detail](https://bitbucket.org/brucematrix/webjet/src/master/movies/views.py).
* Functioning: If provider is not functioning, such as 404 error, response timeout, the data will be retrieved from local file directly. And if the response code is 200 and timeout is not too long(like 2 seconds), the movie data will be rendered to web page and also be written into local server file timely for backup.
* API token:
  1. Create ```.env``` in root to store API token and put this file name in the [.gitignore](https://bitbucket.org/brucematrix/webjet/src/master/movies/.gitignore) to hide API token from public like when git it to Github. To read the API token from ```.env```,  [read_env](https://bitbucket.org/brucematrix/webjet/src/master/movies/utils.py) function was created.
  2. Then in [manage.py](https://bitbucket.org/brucematrix/webjet/src/master/manage.py) and [wsgi.py](https://bitbucket.org/brucematrix/webjet/src/master/movies/wsgi.py), [read_env](https://bitbucket.org/brucematrix/webjet/src/master/movies/utils.py) function was imported .
  3. If a command is executed like ```python manage.py runserver``` etc., or serve it on server with Guicorn execute[wsgi.py](https://bitbucket.org/brucematrix/webjet/src/master/movies/wsgi.py). It will automatically set the token to environment.
  4. Finally, API token will be read in [setting.py](https://bitbucket.org/brucematrix/webjet/src/master/movies/settings.py) by command ```X_ACCESS_TOKEN = os.environ.get("X_ACCESS_TOKEN")```. Then API token could be use by the APP in Django. The format of token in  '''.env'''' should be, eg. ```x-token-access = x12312423523```.
### Prerequisites
This project is running under [python3.6.4](#) environment create by virtualenv.
What things you need to install the packages is kept in [requirements.txt](https://bitbucket.org/brucematrix/webjet/src/master/requirements.txt).
To install in the virtual environment:
```
pip install -r requirements.txt
```
After that, Create ```.env``` file in the root same location as manage.py , and put your API token in, like ```x-token-access = x12312423523```.
## Authors
* **Bruce Li**
## Acknowledgments
* Thanks for review
