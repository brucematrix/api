from django.conf import settings
import requests
import json
import os


X_ACCESS_TOKEN = getattr(settings, 'X_ACCESS_TOKEN') #get token from setting.py
API_ENDPOINT = "http://webjetapitest.azurewebsites.net/api/{}/{}/"
MODULE_DIR = os.path.dirname(__file__) # get current directory

class MovieHandler():
    '''API handler to get access movies data from API or from local file'''
    def __init__(self):
        self.token={
            "x-access-token":X_ACCESS_TOKEN
        }
        self.endpoint = API_ENDPOINT

    #dynamically get uri by passing parametres, movie_id is optional
    def get_endpoint(self, provider, movie_id=None):
        if movie_id is not None:
            return self.endpoint.format(provider,"movie")+movie_id
        return self.endpoint.format(provider,"movies")

    # get the local path which store the movie data
    def get_file_path(self, provider, movie_id=None):
        if movie_id is not None:
            file_path = os.path.join(MODULE_DIR,
                                    "json/{}.txt".format(provider)) \
                                    .replace("\\","/")
        else:
            file_path = os.path.join(MODULE_DIR,
                                    "json/{}/{}.txt".format(provider,
                                                            movie_id),) \
                                    .replace("\\","/")
        return file_path

    def get_file_data(self, file_path):
        if os.path.isfile(file_path):
            with open(file_path, 'r') as f:
                json_data = json.load(f)
        else:
            json_data=None
        return json_data

    #send request with X_ACCESS_TOKEN header
    def send_request(self, provider, movie_id=None):
        response = requests.get(self.get_endpoint(provider, movie_id),
                                headers=self.token,
                                timeout=2) # set timeout as 2 seconds
        return response

    # if provider is functioning, get the timely data, else get local data
    def load_data(self, provider, movie_id=None):
        file_path = self.get_file_path(provider, movie_id)
        print(file_path)
        try:
            response = self.send_request(provider, movie_id)
            print(response)
            print(response.elapsed.total_seconds())
         # catch all request error, including timeout
        except requests.exceptions.RequestException as e:
            # get data from local file
            json_data = self.get_file_data(file_path)
        else:
            if response.status_code == 200:
                json_data = response.json()
                #write latest movies date to file
                with open(file_path, 'w+') as outfile:
                        json.dump(json_data, outfile)
            else:
                json_data = self.get_file_data(file_path)
        return json_data

    #get movie list by provider
    def get_movies(self, provider):
        return self.load_data(provider)

    #get movie details by provide rand movie id
    def get_movie(self, provider, movie_id):
        return self.load_data(provider, movie_id)
