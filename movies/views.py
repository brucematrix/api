from django.shortcuts import render
from .moviehandler import MovieHandler


'''Global Assumption: 1. Same movie from different provider has
same digits included in the movie ID.
'''


'''Display all the unique movies from both providers in home page'''
def movies_list(request):
    cinema_movies_id = []
    film_movies_id = []
    movies = []

    #get the cinema movies and trancate and store its id
    cinema_data = MovieHandler().get_movies("cinemaworld")
    for cinema_movie in cinema_data["Movies"]:
        cinema_movie['ID']=cinema_movie['ID'].replace("cw","")
        movies.append(cinema_movie)
        cinema_movies_id.append(cinema_movie['ID'])

    #get the filmworld movies and trancate and store its id
    film_data = MovieHandler().get_movies("filmworld")
    for film_movie in film_data["Movies"]:
        film_movie['ID']=film_movie['ID'].replace("fw","")
        film_movies_id.append(film_movie['ID'])

    #Get ID of movies is  provide by filmworld only and append to movies
    diff_film = set(film_movies_id).difference(set(cinema_movies_id))
    if len(diff_film) is not 0:
        for id in diff_film:
            for film_movie in film_data["Movies"]:
                if film_movie['ID'] == id:
                    movies.append(film_movie) #get all the unique movies

    return render(request, 'home.html', {"movies": movies,})


'''Load movie detail to movie.html
Asumption:1.Movie detail may differ from different provider itself.
Soit is better to give customer full information from both providers
'''
def movie_detail(request, movie_id):
    cw_id = "cw{}".format(movie_id) #cinemaworld movie id
    fw_id = "fw{}".format(movie_id) #filmworld movie id
    cw_movie = MovieHandler().get_movie("cinemaworld", cw_id)
    fw_movie = MovieHandler().get_movie("filmworld", fw_id)
    context={
        "cw_movie": cw_movie,
        "fw_movie": fw_movie,
    }
    return render(request, "movie.html", context)
