import os

"""Read local default environment variables
from a .env file located in the project root directory.
"""
def read_env(path):
    try:
        with open(path) as f:
            content = f.read()
    except IOError:
        content = ""

    for line in content.splitlines():
        m1 = line.split("=", 1)
        if m1:
            key, value = m1
            key = key.split()[0]
            val = value.split()[0]
            os.environ.setdefault(key, val)
